# CHIRP  
### Radio programming made simple(ish)  

---

* What is it?  
    * Open source software for programming many ham radios.  
* Where can I find it?  
    * https://chirp.danplanet.com/projects/chirp/wiki/Home  

---

* Why do I want it?
    * Ham radio interface designs are terrible making them difficult and cumbersome to program.
    * Programming many frequencies by hand can take a long time. Automating the process with CHIRP will make your life easier.
    * Frequency lists can be exported as CSV files and easily shared.

---  
